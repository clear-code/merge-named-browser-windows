/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import CrossSessionState from '/extlib/CrossSessionState.js';

import {
  log,
  configs,
  KEEP_TAB_MODE_NEW_TAB,
  //KEEP_TAB_MODE_OLD_TAB,
} from './common.js';

const gState = new CrossSessionState({
  nameByTab: {
    default: {},
  },
  tabsByName: {
    default: {},
  },
});

chrome.runtime.onMessage.addListener(async (message, sender) => {
  if (!message ||
      message.type != 'named-window-loaded')
    return;

  log('new named tab is detected!', { tab: sender.tab.id, name: message.name });

  await gState.restored;
  const oldName = gState.nameByTab[sender.tab.id];
  if (oldName &&
      message.name != oldName) {
    const tabs = gState.tabsByName[oldName];
    const index = tabs.indexOf(sender.tab.id);
    tabs.splice(index, 1);
    gState.tabsByName[oldName] = tabs;
  }

  const tabs = gState.tabsByName[message.name] || [];
  if (!tabs.includes(sender.tab.id))
    tabs.push(sender.tab.id);
  gState.tabsByName[message.name] = tabs;

  gState.nameByTab[sender.tab.id] = message.name;

  gState.save('nameByTab', 'tabsByName');
  log(' => ', { nameByTab: gState.nameByTab,  tabsByName: gState.tabsByName });

  if (tabs.length <= 1)
    return;

  log(`there are ${tabs.length} tabs with the name ${message.name}`);
  if (configs.keepTab == KEEP_TAB_MODE_NEW_TAB) {
    log(' => close all except the last one');
    if (configs.replaceOldTab) {
      const [first, last] = await Promise.all([
        chrome.tabs.get(tabs[0]),
        chrome.tabs.get(tabs[tabs.length - 1])
      ]);
      let index = first.index;
      if (first.windowId == last.windowId && first.index > last.index)
        index--;
      const [source, dest] = await Promise.all([
        chrome.windows.get(last.windowId),
        chrome.windows.get(first.windowId),
      ]);
      if (source.type == 'normal' &&
          dest.type == 'normal')
        chrome.tabs.move(last.id, { windowId: first.windowId, index });
    }
    for (const tabId of tabs.slice(0, -1)) {
      chrome.tabs.remove(tabId);
    }
  }
  else { // keep old tab
    log(' => close all except the first one');
    for (const tabId of tabs.slice(1)) {
      chrome.tabs.remove(tabId);
    }
  }
});

chrome.tabs.onRemoved.addListener(async (tabId, _removeInfo) => {
  await gState.restored;
  const name = gState.nameByTab[tabId];
  if (!name)
    return;

  log('named tab is closed: ', tabId);
  const tabs = gState.tabsByName[name];
  log(' => same name tabs: ', tabs);
  if (!tabs || !tabs.includes(tabId))
    return;

  const index = tabs.indexOf(tabId);
  log(' => index: ', index);
  tabs.splice(index, 1);
  if (tabs.length > 0)
    gState.tabsByName[name] = tabs;
  else
    delete gState.tabsByName[name];
  gState.save('tabsByName');
});

gState.restored.then(async () => {
  if (gState.resumed) {
    log('resumed: skip initialization');
    return;
  }

  const tabs = await chrome.tabs.query({});
  for (const tab of tabs) {
    if (!/^https?:/.test(tab.url))
      continue;
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      files: ['./content_script.js'],
    });
  }
});
