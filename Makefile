NPM_MOD_DIR := $(CURDIR)/node_modules
NPM_BIN_DIR := $(NPM_MOD_DIR)/.bin
TIMESTAMP=$(shell date +%Y%m%d)

.PHONY: xpi install_dependency install_hook lint format chromium

all: xpi chromium

install_dependency:
	[ -e "$(NPM_BIN_DIR)/eslint" -a -e "$(NPM_BIN_DIR)/jsonlint-cli" ] || npm install

install_hook:
	echo '#!/bin/sh\nmake lint' > "$(CURDIR)/.git/hooks/pre-commit" && chmod +x "$(CURDIR)/.git/hooks/pre-commit"

lint: install_dependency
	"$(NPM_BIN_DIR)/eslint" . --ext=.js --report-unused-disable-directives
	find . -type d -name node_modules -prune -o -type f -name '*.json' -print | xargs "$(NPM_BIN_DIR)/jsonlint-cli"

format: install_dependency
	"$(NPM_BIN_DIR)/eslint" . --ext=.js --report-unused-disable-directives --fix

xpi: update_extlib install_extlib lint
	rm -f ./*.xpi
	zip -r -9 merge-named-browser-windows.xpi manifest.json background.js common.js content_script.js options extlib _locales -x '*/.*' >/dev/null 2>/dev/null

chromium: update_extlib install_extlib lint
	rm -rf chromium
	mkdir -p chromium
	cat manifest.json | jq 'del(.browser_specific_settings)' | jq '.storage.managed_schema = "managed_schema.json"' > chromium/manifest.json
	cp -r managed_schema.json background.js common.js content_script.js options extlib _locales chromium/
	find chromium -name '.*' | xargs rm -rf
	cd chromium && zip -r ../merge-named-browser-windows-chromium-${TIMESTAMP}.zip .

update_extlib:
	git submodule update --init

install_extlib:
	cp submodules/webextensions-lib-configs/Configs.js extlib/; echo 'export default Configs; const browser = chrome;' >> extlib/Configs.js
	cp submodules/webextensions-lib-cross-session-state/CrossSessionState.js extlib/; echo 'export default CrossSessionState;' >> extlib/CrossSessionState.js
	cp submodules/webextensions-lib-options/Options.js extlib/; echo 'export default Options;' >> extlib/Options.js
	cp submodules/webextensions-lib-l10n/l10n.js extlib/; echo 'export default l10n;' >> extlib/l10n.js

