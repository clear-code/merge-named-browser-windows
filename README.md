# merge-named-browser-windows

In short: this introduces a behavior around named windows on the Internet Explorer.

When a named browser window is opened via JavaScript code like `window.open("...", "subwindow", "width=400,height=400")` Firefox allows to open two or more tabs for same name, if they are opened from different tabs. This extension disallows to open such multiple tabs with same name, instead leave only one tab for each name - newest or oldest.
