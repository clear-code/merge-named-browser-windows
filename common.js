/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import Configs from '/extlib/Configs.js';

export function log(message, ...args) {
  console.log(`[merge-named-browser-windows] ${message} `, ...args);
}

export const KEEP_TAB_MODE_NEW_TAB = 'new';
export const KEEP_TAB_MODE_OLD_TAB = 'old';

export const configs = new Configs({
  keepTab: KEEP_TAB_MODE_NEW_TAB,
  replaceOldTab: true
});
